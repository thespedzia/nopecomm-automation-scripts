#Change Execution Policy
#Set-Executionpolicy -Scope CurrentUser -ExecutionPolicy Unrestricted -Force
Set-ExecutionPolicy Bypass -Scope Process -Force
$AllProtocols = [System.Net.SecurityProtocolType]'Ssl3,Tls,Tls11,Tls12'
[System.Net.ServicePointManager]::SecurityProtocol = $AllProtocols

#Disable Firewall
Set-NetFirewallProfile -Name Domain,Public,Private -Enabled False

#Disable IPv6
Disable-NetAdapterBinding -Name "Ethernet" -ComponentID ms_tcpip6

#Install IIS
# To list all Windows Features: dism /online /Get-Features
# Get-WindowsOptionalFeature -Online 
# LIST All IIS FEATURES: 
# Get-WindowsOptionalFeature -Online | where FeatureName -like 'IIS-*'
# Source: https://weblog.west-wind.com/posts/2017/May/25/Automating-IIS-Feature-Installation-with-Powershell
$arr = "IIS-WebServerRole","IIS-WebServer","IIS-CommonHttpFeatures","IIS-HttpErrors","IIS-HttpRedirect",
        "IIS-ApplicationDevelopment","NetFx4Extended-ASPNET45","IIS-NetFxExtensibility45","IIS-HealthAndDiagnostics",
        "IIS-HttpLogging","IIS-LoggingLibraries","IIS-RequestMonitor","IIS-HttpTracing","IIS-Security","IIS-RequestFiltering",
        "IIS-Performance","IIS-WebServerManagementTools","IIS-IIS6ManagementCompatibility","IIS-Metabase","IIS-ManagementConsole",
        "IIS-BasicAuthentication","IIS-WindowsAuthentication","IIS-StaticContent","IIS-DefaultDocument","IIS-WebSockets",
        "IIS-ApplicationInit","IIS-ISAPIExtensions","IIS-ISAPIFilter","IIS-HttpCompressionStatic","IIS-ASPNET45"

foreach ( $iis_value in $arr)
{
    Enable-WindowsOptionalFeature -Online -FeatureName $iis_value
}

$checkCoreRT = Get-WmiObject -Class Win32_Product | sort-object Name | select Name | where { $_.Name -match ".NET Core Runtime"}

#Installs Chocolatey Windows Package manager
iwr https://chocolatey.org/install.ps1 -UseBasicParsing | iex

#if .net core runtime doesn't exist it is installed
if (! $checkCoreRT)
{
    choco install dotnetcore-runtime -y
}

#Change the machine name and reboot
$newhostname="WIN-SRV-CORE-IIS"
$curhostname=hostname
$localuser="\Administrator"
$localcred=$curhostname+$localuser

if ($newhostname -ne $currenthostname)
{
    Rename-Computer -ComputerName $curhostname -NewName $newhostname -LocalCredential $localcred -Force -PassThru -Restart
    #Change Execution Policy Back
    Set-Executionpolicy -Scope CurrentUser -ExecutionPolicy Restricted -Force
    shutdown /r /t 0
}

